# Copyright 2009 Maxime Coste <frrrwww@gmail.com>
# Copyright 2012 Wouter van Kesteren <woutershep@gmail.com>
# Distributed under the terms of the GNU General Public License v2

MY_PNV=CEGUI-${PV}

require sourceforge [ project=crayzedsgui suffix=tar.gz ]

SUMMARY="Crazy Eddie's video games GUI library"
HOMEPAGE="http://www.cegui.org.uk"

LICENCES="LGPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    devil [[ requires = opengl description = [ Image loading via DevIL ] ]]
    opengl
    (
        expat   [[ description = [ Build expat XML parser module ] ]]
        libxml  [[ description = [ Build libxml XML parser module ] ]]
        tinyxml [[ description = [ Build tinyXML XML parser module ] ]]
        xerces  [[ description = [ Build Xerces-C++ XML parser module ] ]]
    ) [[ number-selected = at-least-one ]]
"

DEPENDENCIES="
    build+run:
        dev-libs/pcre[>=5.0]
        media-libs/freetype:2[>=0.15.0]
        devil? ( media-libs/DevIL )
        expat? ( dev-libs/expat )
        libxml? ( dev-libs/libxml2[>=2.6] )
        opengl? (
            media-libs/glew
            x11-dri/freeglut
            x11-dri/glu
            x11-dri/mesa[>=9]
        )
        tinyxml? ( dev-libs/tinyxml )
        xerces? ( dev-libs/xerces-c )
"

WORK=${WORKBASE}/${MY_PNV}

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-corona
    --disable-directfb-renderer
    --disable-freeimage
    --disable-irrlicht-renderer
    --disable-lua-module
    --disable-samples
    --disable-silly
    --disable-static
    --disable-toluacegui
    --without-gtk2
    --without-ogre-renderer
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    devil
    expat libxml tinyxml "xerces xerces-c"
    "opengl opengl-renderer"
    "opengl tga"
)

DEFAULT_SRC_PREPARE_PATCHES=(
    -p3 "${FILES}"/CEGUIDevILImageCodec.patch
    -p1 "${FILES}"/ptrdiff_t.patch
)

